﻿using Newtonsoft.Json;
using ParkingSystem.Repository;
using ParkingSystem.Repository.Security;
using ParkingSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ParkingSystem.Business_Layer
{
    public class CommonBl
    {
        //For Verification of Logger
        public int VerifyLogger(LoginViewModel model)
        {
            CommonRepository commonRepo = new CommonRepository();
            model.Password = ComputeHash(model.Password, model.UserName);
            return (commonRepo.CheckUserLogger(model));
        }

        //Role Retrival
        public string GetRole(int user)
        {
            CommonRepository commonRepo = new CommonRepository();
            return (commonRepo.GetRole(user));
        }

        //Compute Hash
        public string ComputeHash(string password, string username)
        {
            //Email Id is used as a salt
            byte[] saltBytes = System.Text.Encoding.UTF32.GetBytes(username);
            using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, saltBytes, 1000))
                return (Convert.ToBase64String(rfc2898DeriveBytes.GetBytes(256)));
        }

        //Get Customer Booking History
        public List<DisplayVisitHistoryViewModel> GetBookingHistory(JqGridViewModel model, int personId)
        {
            CustomerRepository customerRepo = new CustomerRepository();
            List<DisplayVisitHistoryViewModel> bookingHistory = new List<DisplayVisitHistoryViewModel>();
            if (model._search)
            {
                switch (model.searchField)
                {
                    case "RegistrationId":
                        bookingHistory = customerRepo.GetBookingHistoryByRegistrationId(personId, model.searchString);
                        return bookingHistory;

                    case "PlotName":
                        bookingHistory = customerRepo.GetBookingHistoryByPlotName(personId, model.searchString);
                        return bookingHistory;

                    case "EntryTime":
                        bookingHistory = customerRepo.GetBookingHistoryByEntryTime(personId, model.searchString);
                        return bookingHistory;
                }
            }
            return (customerRepo.DisplayVisitRecords(personId)); ;
        }

        //Check duplicacy of Email Id
        public bool CheckValidEmail(string UserName)
        {
            CommonRepository commonRepo = new CommonRepository();
            int result = commonRepo.CheckValidEmail(UserName);
            if (result == 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //Displays Fare after Slot Closure
        public List<DisplayPaymentCloseDetails> DisplayFareWhenClose(int slotId)
        {
            StaffRepository staffRepo = new StaffRepository();
            return (staffRepo.DisplayFareWhenClose(slotId));
        }

        //City Dropdown
        public HomePageViewModel GetCityHomeDropdown()
        {
            HomePageViewModel model = new HomePageViewModel();
            CommonRepository commonRepo = new CommonRepository();
            model.CityList = new SelectList(commonRepo.GetCityList(), "cityName", "cityName");
            return model;
        }

        //Displays Plot Details in the popup
        public List<HomePageDisplayViewModel> PlotPopUpDetails(string cityName, string plotName)
        {
            HomePageViewModel model = new HomePageViewModel();
            model.CityName = cityName;
            model.AreaName = plotName;
            CustomerRepository customerRepo = new CustomerRepository();
            var plotId = customerRepo.GetSlotsForPlotAndArea(model);
            CommonRepository commonRepo = new CommonRepository();
            return (commonRepo.GetPlotDetailsForHomePage(plotId));
        }

        //Dropdown Plot Registration
        public PlotRegisterViewModel PlotRegistration()
        {

            CommonRepository commonRepo = new CommonRepository();
            PlotRegisterViewModel model = new PlotRegisterViewModel();
            model.AreaList = new SelectList(commonRepo.GetAreaList(), "areaName", "areaName");
            model.CityList = new SelectList(commonRepo.GetCityList(), "cityName", "cityName");
            return model;
        }

        //Login Post
        public HttpCookie UserData(LoginViewModel model, int user)
        {
            CommonBl commonBl = new CommonBl();
            CommonRepository commonRepo = new CommonRepository();
            var roles = commonRepo.GetRole(user);
            CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
            serializeModel.PersonId = user;
            serializeModel.UserName = model.UserName;
            serializeModel.RoleName = roles;
            string userData = JsonConvert.SerializeObject(serializeModel);
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
            1,
            model.UserName,
            DateTime.Now,
            DateTime.Now.AddMinutes(15),
            false, //pass here true, if you want to implement remember me functionality
            userData);
            string encTicket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            return (faCookie);
        }

        //for the grid Pagination
        public int GridPagination(JqGridViewModel model, int cnt)
        {
            var count = cnt;
            int pageIndex = model.page;
            int pageSize = model.rows;
            int startRow = (pageIndex * pageSize) + 1;
            int totalRecords = count;
            return ((int)Math.Ceiling((float)totalRecords / (float)pageSize));
        }

        public void PasswordChange(PasswordChangeViewModel model)
        {
            CommonRepository commonRepo = new CommonRepository();
            commonRepo.PasswordChange(model);
        }


        //public  async Task<string> ConfirmationEmail()
        //{


        //    string to = "ashiahakim080@gmail.com";
        //    string from = "ashiahakim080@gmail.com";
        //    string subject = "Email Confirmation";
        //    string body = "You are successfully registered in PARK MY CAR.Enjoy Parking!!!!";
        //    MailMessage message = new MailMessage(from, to, subject, body);
        //    message.IsBodyHtml = true;
        //    using (var smtp = new SmtpClient())
        //    {
        //        var credential = new NetworkCredential
        //        {
        //            UserName = "ashiahakim080@gmail.com",  // replace with valid value
        //            Password = "hakimuddin"  // replace with valid value
        //        };
        //        smtp.Credentials = credential;
        //        smtp.Host = "smtp.gmail.com";
        //        smtp.Port = 587;
        //        smtp.EnableSsl = true;
        //        await smtp.SendMailAsync(message);
               

        //    }
        //    return "sent";
        //}

        //public async Task<string> Contact()
        //{
        //    string to = "ashiahakim080@gmail.com";
        //    string from = "ashiahakim080@gmail.com";
        //    string subject = "Email Confirmation";
        //    string body = "You are successfully registered in PARK MY CAR.Enjoy Parking!!!!";
        //    MailMessage message = new MailMessage(from, to, subject, body);
        //    message.IsBodyHtml = true;
        //    using (var smtp = new SmtpClient())
        //    {
        //        var credential = new NetworkCredential
        //        {
        //            UserName = "ashiahakim080@gmail.com",  // replace with valid value
        //            Password = "hakimuddin"  // replace with valid value
        //        };
        //        smtp.Credentials = credential;
        //        smtp.Host = "smtp.gmail.com";
        //        smtp.Port = 587;
        //        smtp.EnableSsl = true;
        //        await smtp.SendMailAsync(message);


        //    }
        //    return "ashia";


        //}
        public void EmailConfirmation(string username)
        {
            string to = username;
            string from = "ashiahakim080@gmail.com";
            string subject = "Email Confirmation";
            string body = "PARK MY CAR::Thankyou for the registration.Enjoy Parking!!!!";
            MailMessage message = new MailMessage(from, to, subject, body);
            message.IsBodyHtml = true;
            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "ashiahakim080@gmail.com",  // replace with valid value
                    Password = "hakimuddin"  // replace with valid value
                };
                smtp.Credentials = credential;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.Send(message);
            }
        }
    }
}