﻿using ParkingSystem.Repository;
using ParkingSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;

namespace ParkingSystem.Business_Layer
{
    public class CustomerBl
    {
        //Customer registration
        public void CustomerRegister(CustomerRegisterViewModel model)
        {
            CommonBl commonuserBl = new CommonBl();
            model.Hash = commonuserBl.ComputeHash(model.Password, model.UserName);
            CustomerRepository customerRepo = new CustomerRepository();
           
           
            customerRepo.AddCustomer(model);
            commonuserBl.EmailConfirmation(model.UserName);
        }

        //slot display for Plot
        public int GetSlotsForPlotAndArea(HomePageViewModel model)
        {
            CustomerRepository customerRepo = new CustomerRepository();
            return (customerRepo.GetSlotsForPlotAndArea(model));

        }

        //Customer Username for autocomplete
        public List<string> GetCustomerUserName(string term)
        {
            CustomerRepository customerRepo = new CustomerRepository();
            return (customerRepo.GetCustomerUserName(term));
        }

        //Slots for searched CityName,PlotName
        public int GetPlotIdForSearched(string cityName, string plotName)
        {
            HomePageViewModel model = new HomePageViewModel();
            model.CityName = cityName;
            model.AreaName = plotName;
            CustomerRepository customerRepo = new CustomerRepository();
            return (customerRepo.GetSlotsForPlotAndArea(model));
        }

        //Edit Customer Details
        public void EditPersonDetail(EditProfileCustomerViewModel model, int personId)
        {
            model.Id = personId;
            CommonRepository commonRepo = new CommonRepository();
            commonRepo.UpdatePersonRecords(model);
        }
    }
}