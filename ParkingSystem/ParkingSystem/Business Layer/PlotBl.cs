﻿using ParkingSystem.Repository;
using ParkingSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ParkingSystem.Business_Layer
{
    public class PlotBl
    {
        //Plot Registration
        public PlotSectionCapacityViewModel PlotRegister(PlotRegisterViewModel model)
        {
            PlotSectionCapacityViewModel sectionModel = new PlotSectionCapacityViewModel();
            CommonBl commonBl = new CommonBl();
            model.Hash = commonBl.ComputeHash(model.Password, model.UserName);
            PlotRepository plotRepo = new PlotRepository();
            sectionModel.PlotId = plotRepo.AddPlot(model);
            commonBl.EmailConfirmation(model.UserName);
            return sectionModel;
        }

        //Plot Details of Plotter
        public List<DisplayPlotNamePerPerson> GetPlotDetailsForPlotter(int personId)
        {
            PlotRepository plotRepo = new PlotRepository();
            return (plotRepo.GetPlotDetailsForPerson(personId));
        }
       

        //Booking of Slot
        public int BookSlot(SlotRegistrationViewModel model, int personId)
        {
            model.BookerId = personId;
            PlotRepository plotRepo = new PlotRepository();
            plotRepo.RegisterSlotForCustomer(model);
            return (plotRepo.GetPlotIdFromSlotId(model.SlotId));
        }
        


       /// <summary>
       /// 
       /// </summary>
       /// <param name="model"></param>
       /// <param name="personId"></param>
       /// <returns></returns>
        public List<DisplayStaffDetails> GetStaffDetails(JqGridViewModel model, int personId)
        {
            PlotRepository plotRepo = new PlotRepository();
            List<DisplayStaffDetails> staffList = new List<DisplayStaffDetails>();
            if (model._search)
            {
                switch (model.searchField)
                {
                    case "StaffName":
                        staffList = plotRepo.GetStaffDetailsByStaffName(personId, model.searchString);
                        return staffList;

                    case "PlotName":
                        staffList = plotRepo.GetStaffDetailsByPlotName(personId, model.searchString);
                        return staffList;

                    case "UserName":
                        staffList = plotRepo.GetStaffDetailsByUserName(personId, model.searchString);
                        return staffList;
                }
            }
            return (plotRepo.GetStaffDetailsOfPlotter(personId));
        }

        //Get Booking Details of Plotter Based on Search
        public List<BookingDetailsViewModel> GetBookingDetailsOfPlotter(JqGridViewModel model, int personId)
        {
            PlotRepository plotRepo = new PlotRepository();
            List<BookingDetailsViewModel> bookingList = new List<BookingDetailsViewModel>();
            if (model._search)
            {
                switch (model.searchField)
                {
                    case "PlotName":
                        bookingList = plotRepo.GetBookingDetailsByPlotName(personId, model.searchString);
                        return bookingList;

                    case "EntryTime":
                        bookingList = plotRepo.GetBookingDetailsByEntryTime(personId, model.searchString);
                        return bookingList;

                    case "RegistrationId":
                        bookingList = plotRepo.GetBookingDetailsByReg(personId, model.searchString);
                        return bookingList;
                }
            }
            return (plotRepo.GetBookingDetailsOfPlotter(personId));
        }

        //Validation of Capacity
        public string CheckCapacityDetails(PlotSectionCapacityViewModel model, string id)
        {
            if (model.Capacity >= 0)
            {
                PlotRepository plotrepo = new PlotRepository();
                int plotId;
                if (int.TryParse(id, out plotId))
                {
                    model.PlotId = plotId;
                    plotrepo.EnterNewSectionCapacity(model);
                    return ("Saved Successfully");
                }
                return ("Data not Saved");
            }
            else
            {
                return ("Capacity cannot be negative");
            }
        }

        //Get Section Details
        public List<DisplaySlotNamePerSection> GetplotSectionDetails(int plotId)
        {
            PlotRepository plotRepo = new PlotRepository();
            return (plotRepo.GetSectionDetailsForPlot(plotId));
        }

        //Get PlotId from SlotId
        //public int GetplotIdFromSlot(string id)
        //{
        //    int slotId;
        //    bool result = int.TryParse(id, out slotId);
        //    if (result)
        //    {
        //        PlotRepository plotRepo = new PlotRepository();
        //        return (plotRepo.GetPlotIdFromSlotId(slotId));
        //    }
        //    return 0;
        //}

        public int GetplotIdFromSlot(int id)
        {
          
                PlotRepository plotRepo = new PlotRepository();
                return (plotRepo.GetPlotIdFromSlotId(id));
           
           
        }


        //adding dropodown in add more plot page
        public AddMorePlot AddMorePlot()
        {
            AddMorePlot model = new AddMorePlot();
            CommonRepository commonRepo = new CommonRepository();
            model.AreaList = new SelectList(commonRepo.GetAreaList(), "areaName", "areaName");
            model.CityList = new SelectList(commonRepo.GetCityList(), "cityName", "cityName");
            return model;
        }

        //Check for Capacity Details
        public string AddExistingPlotSection(DisplayCapacityForEditViewModel model, string id)
        {
            if (model.Capacity >= 0)
            {
                int plotId;
                bool result = int.TryParse(id, out plotId);
                if (result)
                {
                    model.PlotId = plotId;
                    PlotRepository plotrepo = new PlotRepository();
                    plotrepo.AddSectionInExistingPlot(model);
                    return ("Saved Successfully");
                }
                else
                {
                    return (" Data not saved");
                }
            }
            else
            {
                return ("Capacity Cannot be negative");
            }
        }

        //Plot Details Edit
        public void PlotEditDetails(EditPlotDetailsViewModel model)
        {
            PlotRepository plotRepo = new PlotRepository();
            plotRepo.EditPlotLocationDetails(model);
        }

        // Validation of Capacity Details
        public string PlotEditSection(DisplayCapacityForEditViewModel model)
        {
            if (model.Capacity >= 0)
            {
                PlotRepository plotRepo = new PlotRepository();
                plotRepo.EditPlotCapacityDetails(model);
                return ("Saved Successfully");
            }
            return ("Capacity cannot be negative");
        }

        //Slot List for Customer
        public List<DisplaySlotNamePerSection> SlotList(string id)
        {
            PlotRepository plotRepo = new PlotRepository();
            int plotId;
            if (int.TryParse(id, out plotId))
            {
                return (plotRepo.GetSectionDetailsForPlot(plotId));
            }
            return null;
        }

        //Validation of Plot Capacity
        public string CapacityValidationMessage(PlotSectionCapacityViewModel model, string id)
        {
            if (model.Capacity >= 0)
            {
                int plotId;
                if (int.TryParse(id, out plotId))
                {
                    model.PlotId = plotId;
                    PlotSectionCapacityViewModel plotter = new PlotSectionCapacityViewModel();
                    PlotRepository plotRepo = new PlotRepository();
                    plotRepo.EnterNewSectionCapacity(model);
                    return ("Saved Successfully");
                }
                return ("Data not Saved");
            }
            return ("Capacity cannot be negative");
        }


    }
}