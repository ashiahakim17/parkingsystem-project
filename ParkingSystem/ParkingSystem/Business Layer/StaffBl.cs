﻿using ParkingSystem.Repository;
using ParkingSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ParkingSystem.Business_Layer
{
    public class StaffBl
    {

        //Staff Booking History
        public List<BookingDetailsViewModel> StaffBookingHistorySearch(JqGridViewModel model, int personId)
        {
            StaffRepository staffRepo = new StaffRepository();
            List<BookingDetailsViewModel> bookingList = new List<BookingDetailsViewModel>();
            if (model._search)
            {
              
                switch (model.searchField)
                {
                    case "RegistrationId":
                        bookingList = staffRepo.GetStaffBookingByReg(personId, model.searchString);
                        return bookingList;

                    case "EntryTime":
                        bookingList = staffRepo.GetStaffBookingByDate(personId, model.searchString);
                        return bookingList;
                    case "UserName":
                        bookingList = staffRepo.GetStaffBookingByUserName(personId, model.searchString);
                        return bookingList;
                }
            }  
            return (staffRepo.GetBookingDetailsOfStaff(personId));
        }

        //Staff Registration Plot dropdown
        public StaffRegisterViewModel StaffRegisterDropdown(int personId)
        {
            StaffRegisterViewModel model = new StaffRegisterViewModel();
            StaffRepository staffRepo = new StaffRepository();
            model.PlotList = new SelectList(staffRepo.GetPlotList(personId), "plotName", "plotName");
            return model;

        }

        //Staff Registration
        public void StaffRegister(StaffRegisterViewModel model, int personId)
        {
            CommonBl commonBl = new CommonBl();
            model.Hash = commonBl.ComputeHash(model.Password, model.UserName);
            StaffRepository staffRepo = new StaffRepository();
            model.PlotterId = personId;
            staffRepo.AddStaff(model);
            commonBl.EmailConfirmation(model.UserName);
        }

        //Retrival of Staff Slots
        public List<DisplaySlotNamePerSection> GetSlotsOfStaff(int personId)
        {
            StaffRepository staffRepo = new StaffRepository();
            return (staffRepo.GetSlotDetailsOfStaff(personId));
        }

        //Plot Details of Staff
        public List<DisplayPlotNamePerPerson> GetPlotDetailsOfStaff(int personId)
        {
            StaffRepository staffRepo = new StaffRepository();
            return (staffRepo.GetPlotDetailsForPerson(personId));
        }

    }
}