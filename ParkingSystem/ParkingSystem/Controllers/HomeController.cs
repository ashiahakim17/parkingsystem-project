﻿using log4net;
using ParkingSystem.Business_Layer;
using ParkingSystem.Repository;
using ParkingSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

namespace ParkingSystem.Controllers
{
    [AllowAnonymous]
    public class HomeController : BaseController
    {
        private ILog Log = LogManager.GetLogger(typeof(HomeController));

        // GET: Home
        public ActionResult Index()
        {
            if (User != null)
            {
                ViewBag.Roles = User.roles;
            }
            CommonBl commonBl = new CommonBl();
            return View(commonBl.GetCityHomeDropdown());
        }

        //Index Page Post
        [HttpPost]
        public ActionResult Index(HomePageViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    CommonRepository commonRepo = new CommonRepository();
                    return View("DisplayPlotDetails", commonRepo.GetPlotDetailsBySearchedTerm(model));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return View();
        }

        //Displays Plot Details
        public ActionResult DisplayPlotDetails()
        {
            return View();
        }

        //Plot Details For Popup
        public ActionResult PlotDetailsForPopUp(string cityName, string plotName)
        {
            CommonBl commonBl = new CommonBl();
            return Json(commonBl.PlotPopUpDetails(cityName, plotName), JsonRequestBehavior.AllowGet);
        }

        //For Customer Registration
        public ActionResult AddCustomer(string message)
        {
            ViewBag.Message = message;
            return View("~/Views/Customer/AddCustomer.cshtml");
        }

        //Customer Registration Post
        [HttpPost]
        public ActionResult AddCustomer(CustomerRegisterViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    CommonBl commonBl = new CommonBl();
                    bool validEmail = commonBl.CheckValidEmail(model.UserName);
                    if (validEmail)
                    {
                        CustomerBl customerBl = new CustomerBl();
                        customerBl.CustomerRegister(model);
            
                        return RedirectToAction("AddCustomer", "Home", new { message = model.Name + ", you are sucessfully registered. " });
                    }
                    ModelState.AddModelError("", "This Email Id is already registered.");
                    return View("~/Views/Customer/AddCustomer.cshtml", model);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return View("~/Views/Customer/AddCustomer.cshtml");
        }

        //Registration page for adding Plot
        public ActionResult RegisterPlot()
        {
            CommonBl commonBl = new CommonBl();
            return View("~/Views/Plot/RegisterPlot.cshtml", commonBl.PlotRegistration());
        }

        [HttpPost]
        //For the Grid View
        public ActionResult PlotSectionGrid(PlotRegisterViewModel model)
        {
            CommonBl commonBl = new CommonBl();
            try
            {
                if (ModelState.IsValid)
                {
                    if (commonBl.CheckValidEmail(model.UserName))
                    {
                        PlotBl plotBl = new PlotBl();
                        return View(plotBl.PlotRegister(model));
                    }
                    ViewBag.Message="This Email Id is already registered.";
                    return View("~/Views/Plot/RegisterPlot.cshtml", commonBl.PlotRegistration());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
            }
            return View("~/Views/Plot/RegisterPlot.cshtml", commonBl.PlotRegistration());
        }

        //For adding the capacity details through the grid view
        [HttpPost]
        public string Create([Bind(Exclude = "Id")] PlotSectionCapacityViewModel model)
        {
            string msg;
            try
            {
                if (ModelState.IsValid)
                {
                    PlotBl plotBl = new PlotBl();
                    msg = plotBl.CapacityValidationMessage(model, this.Request.QueryString["id"]);
                }
                else
                {
                    msg = "Not Saved";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }
            return msg;
        }

        //Get the Capacity Details of the Plot Entered through Grid and displays in the Grid View
        public JsonResult GetSectionCapacityOfPlot(int rows)
        {
           // ViewBag.Role = User.roles;
            PlotRepository plotRepo = new PlotRepository();
            int plotId;
            if (int.TryParse((this.Request.QueryString["id"]), out plotId))
            {
                List<PlotSectionCapacityViewModel> sectionList = plotRepo.GetSectionCapacityOfPlotEntered(plotId);
                var jsonData = new
                            {
                                records = sectionList.Count(),
                                rows = sectionList
                            };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        //Login Page
        public ActionResult Login()
        {
            return View();
        }
        // Login Post

        //[HttpPost]
        //public ActionResult Login(LoginViewModel model, string returnUrl = "")
        //{
        //    CommonBl commonBl = new CommonBl();

        //    if (ModelState.IsValid)
        //    {
               
        //        var user = commonBl.VerifyLogger(model);

        //        if (user > 0)
        //        {
        //            var roles = commonBl.GetRole(user);
        //            Response.Cookies.Add(commonBl.UserData(model, user));
        //            if (roles.Contains("Customer"))
        //            {
        //                return RedirectToAction("Index", "Home");
        //            }
        //            else if (roles.Contains("Plotter"))
        //            {
        //                return RedirectToAction("Index", "Plot");
        //            }
        //            else
        //            {
        //                return RedirectToAction("Index", "Staff");
        //            }
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", "Enter Valid Username and Password.");
        //            return View(model);
        //        }
        //    }
        //    return View(model);
        //}

        //AutoComplete the text Entered in the Text Box
        public ActionResult GetPlotNameAndArea(string term, string city)
        {
            PlotRepository plotRepo = new PlotRepository();
            List<string> areaAutocomplete = plotRepo.GetPlotNameAndArea(term, city);
            return this.Json(areaAutocomplete, JsonRequestBehavior.AllowGet);
        }

        //For the Signout
        [AllowAnonymous]
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Error()
        {
            if (User != null)
            {
                ViewBag.Roles = User.roles;
            }
            return View();
        }

        public ActionResult test()
        {
            if (User != null)
            {
                if (User.roles == "Plotter")
                {
                    return PartialView("_PlotterHeader");
                }
                if (User.roles == "Staff")
                {
                    return PartialView("_StaffHeader");
                }
                if (User.roles == "Customer")
                {
                    return PartialView("_CustomerHeader");
                }
            }

            return PartialView("_HomeHeader");

        }

        public ActionResult LoginVal(string username, string password, string returnUrl = "")
        {
            LoginViewModel model = new LoginViewModel();
            model.UserName = username;
            model.Password = password;
            CommonBl commonBl = new CommonBl();

            if (ModelState.IsValid)
            {

                var user = commonBl.VerifyLogger(model);

                if (user > 0)
                {
                    var roles = commonBl.GetRole(user);
                    Response.Cookies.Add(commonBl.UserData(model, user));
                    if (roles.Contains("Customer"))
                    {
                        return Json("Customer", JsonRequestBehavior.AllowGet);
                    }
                    else if (roles.Contains("Plotter"))
                    {
                        return Json("Plotter", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Staff", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json("Invalid", JsonRequestBehavior.AllowGet);
        }

        public async Task<string> Contact()
        {
            string to = "ashiahakim080@gmail.com";
            string from = "ashiahakim080@gmail.com";
            string subject = "Email Confirmation";
            string body = "You are successfully registered in PARK MY CAR.Enjoy Parking!!!!";
            MailMessage message = new MailMessage(from, to, subject, body);
            message.IsBodyHtml = true;
            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "ashiahakim080@gmail.com",  // replace with valid value
                    Password = "hakimuddin"  // replace with valid value
                };
                smtp.Credentials = credential;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);
               

            }
            return "ashia";
           
          
        }

        public ActionResult Sent()
        {
            return View();

        }
    }
}