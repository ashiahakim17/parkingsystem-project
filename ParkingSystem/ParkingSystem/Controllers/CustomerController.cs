﻿using log4net;
using ParkingSystem.Business_Layer;
using ParkingSystem.Repository;
using ParkingSystem.Repository.Security;
using ParkingSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ParkingSystem.Controllers
{
    public class CustomerController : BaseController
    {
        private ILog Log = LogManager.GetLogger(typeof(CustomerController));

        //Customer Index Page
        [CustomAuthorize(Role.Customer)]
        public ActionResult Index()
        {
          
            CommonBl commonBl = new CommonBl();
            return View("~/Views/Home/Index.cshtml",commonBl.GetCityHomeDropdown());
        }

        //Get Plot Details for autocomplete
        public ActionResult GetPlotDetails(string cityName, string plotName)
        {
            
            CustomerBl customerBl = new CustomerBl();
            return Json(customerBl.GetPlotIdForSearched(cityName, plotName), JsonRequestBehavior.AllowGet);
        }

        // Displays Slots
        [CustomAuthorize(Role.Customer,Role.Staff,Role.Plotter)]
        public ActionResult DisplaySlot(string id)
        {
            ViewBag.roles = User.roles;
            PlotBl plotBl = new PlotBl();
            return View("DisplaySlot", plotBl.SlotList(id));
        }

        // Displays Edit Profile Form
        [CustomAuthorize(Role.Customer)]
        public ActionResult EditProfile()
        {
            ViewBag.roles = User.roles;
            CustomerRepository customerRepo = new CustomerRepository();
            return View(customerRepo.GetPersonDetailsForEditing().Find(Person => Person.Id == User.PersonId));
        }

        //Edit Form Post
        [HttpPost]
        public ActionResult EditProfile(EditProfileCustomerViewModel model)
        {
            ViewBag.roles = User.roles;
            try
            {
                if (ModelState.IsValid)
                {
                    CustomerBl customerBl = new CustomerBl();
                    customerBl.EditPersonDetail(model, User.PersonId);
                    ViewBag.Message = "Details Successfully Updated";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
            }
            return View();
        }

        //Displays Booking History of the Customer
        [CustomAuthorize(Role.Customer)]
        public ActionResult DisplayVisitRecordsForCustomer()
        {
            ViewBag.roles = User.roles;
            return View();
        }

        //Get the Booking History of the Customer
        public JsonResult GetBookingHistoryOfCustomer(JqGridViewModel model)
        {
            CommonBl commonBl = new CommonBl();
            List<DisplayVisitHistoryViewModel> bookingList = commonBl.GetBookingHistory(model, User.PersonId);
            var jsonData = new
            {
                total = commonBl.GridPagination(model, bookingList.Count()),
                page = model.page,
                records = bookingList.Count(),
                rows = bookingList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        //Change Password
         [CustomAuthorize(Role.Customer)]
        public ActionResult ChangePassword()
        {
            ViewBag.roles = User.UserName;
            return View("_ChangePassword");
        }

        [HttpPost]
        public ActionResult ChangePassword(PasswordChangeViewModel model)
        {
           
            try
            {
                if (ModelState.IsValid)
                {
                    model.UserName = User.UserName;
                    CommonBl commonuserBl = new CommonBl();
                    model.NewPassword = commonuserBl.ComputeHash(model.ConfirmPassword, model.UserName);
                    commonuserBl.PasswordChange(model);
                    ViewBag.Message = "Details Successfully Updated";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
            }
            return View("_ChangePassword");
        }
    }
}