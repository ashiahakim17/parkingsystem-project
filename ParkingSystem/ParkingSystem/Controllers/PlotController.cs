﻿using log4net;
using ParkingSystem.Business_Layer;
using ParkingSystem.Repository;
using ParkingSystem.Repository.Security;
using ParkingSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ParkingSystem.Controllers
{
    public class PlotController : BaseController
    {
        private ILog Log = LogManager.GetLogger(typeof(PlotController));

        //Index Page of the Plotter
        [CustomAuthorize(Role.Plotter)]
        public ActionResult Index()
        {
            ViewBag.roles = User.roles;
            PlotBl plotBl = new PlotBl();
            return View("DisplayPlot", plotBl.GetPlotDetailsForPlotter(User.PersonId));
        }

        public ActionResult DisplayPlot()
        {
            PlotBl plotBl = new PlotBl();
            return View("_DisplayPlot", plotBl.GetPlotDetailsForPlotter(User.PersonId));
        }

        //For Displaying the Slots present in the selected Plot
        [CustomAuthorize(Role.Plotter, Role.Staff)]
        public ActionResult DisplaySlot(int plotId)
        {
            ViewBag.roles = User.roles;
            PlotBl plotBl = new PlotBl();
            return PartialView("_DisplaySlot", plotBl.GetplotSectionDetails(plotId));
        }

        //Registration Page for the Slot Booking
        [CustomAuthorize(Role.Plotter, Role.Staff)]
        public ActionResult BookSlot(int slotId)
        {
            return PartialView("_BookSlot");
            //ViewBag.roles = User.roles;
            //if (User.roles == "Plotter")
            //{
            //    return View();
            //}
            //return PartialView("~/Views/Staff/BookSlot.cshtml");
        }

        //After the Submit Button Click(Slot Booking)
        [HttpPost]
        public ActionResult BookSlot(SlotRegistrationViewModel model)
        {
            ViewBag.roles = User.roles;
            try
            {
                if (ModelState.IsValid)
                {
                    PlotBl plotBl = new PlotBl();
                    int plotId = plotBl.BookSlot(model, User.PersonId);
                    return RedirectToAction("DisplaySlot", "Plot", new { plotId = plotId });

                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);

            }
            return View();
        }

        //For Closing the Slot
        [CustomAuthorize(Role.Plotter, Role.Staff)]
        public ActionResult CloseSlot(int slotId)
        {
            ViewBag.roles = User.roles;
            CommonBl commonBl = new CommonBl();

            return PartialView("_CloseSlot", commonBl.DisplayFareWhenClose(slotId));

        }

        //public ActionResult CloseSlotPayment(int slotId)
        //{
        // PlotRepository plotRepo = new PlotRepository();          
        //return PartialView("DisplaySlot",plotRepo.GetSectionDetailsForPlot(plotId));
        //}

        //[HttpPost]
        //public ActionResult CloseSlot()
        //{
        //    ViewBag.roles = User.roles;
        //    try
        //    {
        //        PlotBl plotBl = new PlotBl();
        //        string slotId = Request.Form["slotId"];
        //        int plotId = plotBl.GetplotIdFromSlot(slotId);
        //        if (plotId > 0)
        //        {
        //            if (User.roles == "Plotter")
        //            {
        //                return RedirectToAction("DisplaySlot", "Plot", new { plotId = plotId });
        //            }
        //            return RedirectToAction("DisplaySlot", "Staff", new { plotId = plotId });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex.Message, ex);

        //    }
        //    return View("~/Views/Shared/Error.cshtml");
        //}

        public ActionResult CloseSlotPayment(int slotId)
        {
            //ViewBag.roles = User.roles;
            try
            {
                PlotBl plotBl = new PlotBl();
                //string slotId = Request.Form["slotId"];
                int plotId = plotBl.GetplotIdFromSlot(slotId);
                PlotRepository plotRepo = new PlotRepository();
                if (plotId > 0)
                {
                    return PartialView("_DisplaySlot", plotRepo.GetSectionDetailsForPlot(plotId));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);

            }
            return View("~/Views/Shared/Error.cshtml");
        }

        //For Adding More Plot by the Plotter
        [CustomAuthorize(Role.Plotter)]
        public ActionResult AddMorePlot()
        {
            ViewBag.roles = User.roles;
            PlotBl plotBl = new PlotBl();
            return View(plotBl.AddMorePlot());
        }

        [HttpPost]
        //For the Grid View
        [CustomAuthorize(Role.Plotter)]
        public ActionResult PlotSectionGrid(AddMorePlot model)
        {
            ViewBag.roles = User.roles;
            try
            {
                if (ModelState.IsValid)
                {
                    PlotRepository plotRepo = new PlotRepository();
                    model.PersonId = User.PersonId;
                    PlotSectionCapacityViewModel plotModel = new PlotSectionCapacityViewModel();
                    int id = plotRepo.AddMorePlot(model);
                    if (id > 0)
                    {
                        plotModel.PlotId = id;
                        return View(plotModel);
                    }
                    return View("~/Views/Shared/Error.cshtml");
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);

            }
            return View("AddMorePlot");
        }

        //For Adding the Capacity details through Grid
        [HttpPost]
        public string Create([Bind(Exclude = "Id")] PlotSectionCapacityViewModel model)
        {
            string msg;
            try
            {
                if (ModelState.IsValid)
                {
                    PlotBl plotBl = new PlotBl();
                    msg = plotBl.CheckCapacityDetails(model, this.Request.QueryString["id"]);
                }
                else
                {
                    msg = "Data not successfully submitted";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                msg = "Error occured:" + ex.Message;
            }
            return msg;
        }

        //For Getting the Capacity Details of the Plot Entered
        public JsonResult GetSectionCapacityOfPlot(int rows)
        {
            PlotRepository plotrepo = new PlotRepository();
            int plotId;
            if (int.TryParse((this.Request.QueryString["id"]), out plotId))
            {
                List<PlotSectionCapacityViewModel> sectionList = plotrepo.GetSectionCapacityOfPlotEntered(plotId);
                var jsonData = new
                {
                    records = sectionList.Count(),
                    rows = sectionList
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string AddSectionInExistingPlot([Bind(Exclude = "PlotSectionId")] DisplayCapacityForEditViewModel model)
        {

            string msg;
            try
            {
                if (ModelState.IsValid)
                {
                    PlotBl plotBl = new PlotBl();
                    msg = plotBl.AddExistingPlotSection(model, this.Request.QueryString["id"]);
                }
                else
                {
                    msg = "Data not Saved";
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
            }
            return msg;
        }

        //Shows the list of plot for editing
        [CustomAuthorize(Role.Plotter)]
        public ActionResult DisplayPlotForEditing()
        {
            ViewBag.roles = User.roles;
            PlotRepository plotRepo = new PlotRepository();
            return View(plotRepo.GetPlotDetailsForPerson(User.PersonId));
        }

        //Opens the form For Editing the section details and other details
        [CustomAuthorize(Role.Plotter)]
        public ActionResult EditPlotDetails(int plotId, string message)
        {
            ViewBag.roles = User.roles;
            PlotRepository plotRepo = new PlotRepository();
            EditPlotDetailsViewModel model = new EditPlotDetailsViewModel();
            CommonRepository commonRepo = new CommonRepository();
            ViewBag.AreaList = new SelectList(commonRepo.GetAreaList(), "areaName", "areaName");
            ViewBag.CityList = new SelectList(commonRepo.GetCityList(), "cityName", "cityName");
            ViewBag.Message = message;
            return PartialView(plotRepo.GetPlotDetailsForEditing().Find(Plot => Plot.PlotId == plotId));
        }

        [HttpPost]
        public ActionResult EditPlotDetails(EditPlotDetailsViewModel model)
        {
            ViewBag.roles = User.roles;
            try
            {
                PlotBl plotBl = new PlotBl();
                plotBl.PlotEditDetails(model);
                return RedirectToAction("EditPlotDetails", "Plot", new { plotId = model.PlotId, message = "Details Saved Successfully" });
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        //For editing the capacity details
        [CustomAuthorize(Role.Plotter)]
        public string Edit(DisplayCapacityForEditViewModel model)
        {
            string msg;
            try
            {
                if (ModelState.IsValid)
                {
                    PlotBl plotBl = new PlotBl();
                    return (plotBl.PlotEditSection(model));
                }
                else
                {
                    msg = "Data not successfully Saved";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                msg = "Error occured:" + ex.Message;
            }
            return msg;
        }

        //Get Section Capacity of registered Plot
        public JsonResult GetSectionCapacityOfPlotRegisterd()
        {
            PlotRepository plotrepo = new PlotRepository();
            int plotId;
            if (int.TryParse((this.Request.QueryString["id"]), out plotId))
            {
                List<DisplayCapacityForEditViewModel> sectionlist = plotrepo.GetSectionCapacityOfPlotRegisterd(plotId);
                var jsonData = new
                {
                    records = sectionlist.Count(),
                    rows = sectionlist
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        //Displays the Staff Details of Plotter
        public ActionResult StaffDetails()
        {
            ViewBag.roles = User.roles;
            return View();
        }

        //Retrival of Staff Details of Plotter
        public JsonResult GetStaffDetailsOfPlotter(JqGridViewModel model)
        {

            CommonBl commonBl = new CommonBl();
            PlotBl plotBl = new PlotBl();
            List<DisplayStaffDetails> staffList = plotBl.GetStaffDetails(model, User.PersonId);
            var jsonData = new
            {
                total = commonBl.GridPagination(model, staffList.Count()),
                page = model.page,
                records = staffList.Count(),
                rows = staffList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        // Displays Booking List of Plotter
        public ActionResult BookingListOfPlotter()
        {
            ViewBag.roles = User.roles;
            return View();
        }

        //Retrival of Plotter Booking List
        public JsonResult GetBookingDetailsOfPlotter(JqGridViewModel model)
        {
            PlotBl plotBl = new PlotBl();
            CommonBl commonBl = new CommonBl();
            List<BookingDetailsViewModel> bookingList = plotBl.GetBookingDetailsOfPlotter(model, User.PersonId);
            var jsonData = new
            {
                total = commonBl.GridPagination(model, bookingList.Count()),
                page = model.page,
                records = bookingList.Count(),
                rows = bookingList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        //Autocomplete for Customer EmailId
        public ActionResult Check(string term)
        {
            CustomerBl customerBl = new CustomerBl();
            List<string> customerEmail = customerBl.GetCustomerUserName(term);
            return this.Json(customerEmail, JsonRequestBehavior.AllowGet);
        }

        //Registration form for the staff
        [CustomAuthorize(Role.Plotter)]
        public ActionResult RegisterStaff(string message)
        {
            ViewBag.roles = User.roles;
            if (User.PersonId > 0)
            {
                StaffBl staffBl = new StaffBl();
                ViewBag.Message = message;
                return View("~/Views/Staff/RegisterStaff.cshtml", staffBl.StaffRegisterDropdown(User.PersonId));
            }
            return View("~/Views/Shared/Error.cshtml");
        }

        //After the Submit Button in Register Staff
        [HttpPost]
        public ActionResult RegisterStaff(StaffRegisterViewModel model)
        {
            ViewBag.roles = User.roles;
            try
            {
                StaffBl staffBl = new StaffBl();
                if (ModelState.IsValid)
                {
                    CommonBl commonBl = new CommonBl();
                    if (commonBl.CheckValidEmail(model.UserName))
                    {
                        staffBl.StaffRegister(model, User.PersonId);
                        return RedirectToAction("RegisterStaff", "Plot", new { message = "Form Successfully submitted." });
                    }
                    return RedirectToAction("RegisterStaff", "Plot", new { message = "This Email Id is already registered." });
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);

            }
            return RedirectToAction("RegisterStaff", "Plot", new { message = "Form not sucessfully submitted" });
        }
    }
}