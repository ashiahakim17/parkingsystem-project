﻿using log4net;
using ParkingSystem.Business_Layer;
using ParkingSystem.Repository;
using ParkingSystem.Repository.Security;
using ParkingSystem.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ParkingSystem.Controllers
{
    public class StaffController : BaseController
    {
        private ILog Log = LogManager.GetLogger(typeof(StaffController));

        // GET: Staff
        [CustomAuthorize(Role.Staff)]
        public ActionResult Index()
        {
            ViewBag.roles = User.roles;
            if (User.PersonId > 0)
            {
                StaffBl staffBl = new StaffBl();
                return View("DisplaySlot", staffBl.GetSlotsOfStaff(User.PersonId));
            }
            return View("~/Views/Shared/Error.cshtml");
        }

        //For displaying the plots in which the staff is registered
        [CustomAuthorize(Role.Staff)]
        public ActionResult DisplayPlot()
        {
            ViewBag.roles = User.roles;
            StaffBl staffBl = new StaffBl();
            return View(staffBl.GetPlotDetailsOfStaff(User.PersonId));
        }

        //For Displaying the Slots under that Plot
        //[CustomAuthorize(Role.Staff,Role.Plotter)]
        //public ActionResult DisplaySlot(int plotId)
        //{
        //    ViewBag.roles = User.roles;
        //    PlotRepository plotRepo = new PlotRepository();          
        //    return PartialView("_DisplaySlot",plotRepo.GetSectionDetailsForPlot(plotId));
        //}

        //Displays Staff Booking List
        [CustomAuthorize(Role.Staff)]
        public ActionResult BookingListOfStaff()
        {
            ViewBag.roles = User.roles;
            return View();
        }

        // Retrival of Staff Booking Details
        public JsonResult GetBookingDetailsOfStaff(JqGridViewModel model)
        {
            StaffBl staffBl = new StaffBl();
            CommonBl commonBl = new CommonBl();
            List<BookingDetailsViewModel> bookingList = staffBl.StaffBookingHistorySearch(model, User.PersonId);
            var jsonData = new
            {
                total = commonBl.GridPagination(model, bookingList.Count()),
                page = model.page,
                records = bookingList.Count(),
                rows = bookingList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}