﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParkingSystem.Models
{
    public class UserRoles
    {
        [Key]
        public int PersonId { get; set; }
        [Key]
        public int RoleId { get; set; }
      
    }
}