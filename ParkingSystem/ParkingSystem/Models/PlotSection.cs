﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParkingSystem.Models
{
    public class PlotSection
    {
        [Key]
        public int PlotSectionId { get; set; }
        public int PlotId { get; set; }
        public string SectionName { get; set; }
        public int Capacity { get; set; }
    }
}