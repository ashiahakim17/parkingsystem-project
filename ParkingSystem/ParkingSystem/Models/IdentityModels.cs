﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ParkingSystem.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.CustomerDetailsViewModel> CustomerDetailsViewModels { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.CustomerRegisterViewModel> CustomerRegisterViewModels { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.PlotRegisterViewModel> PlotRegisterViewModels { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.StaffRegisterViewModel> StaffRegisterViewModels { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.DisplayPlotDetailsViewModel> DisplayPlotDetailsViewModels { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.LoginViewModel> LoginViewModels { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.PlotSectionCapacityViewModel> PlotSectionCapacityViewModels { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.DisplaySectionDetailsViewModel> DisplaySectionDetailsViewModels { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.DisplaySlotNamePerSection> DisplaySlotNamePerSections { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.DisplayPlotNamePerPerson> DisplayPlotNamePerPersons { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.SlotRegistrationViewModel> SlotRegistrationViewModels { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.DisplayPaymentCloseDetails> DisplayPaymentCloseDetails { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.AddMorePlot> AddMorePlots { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.Models.Area> Areas { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.DisplayVisitHistoryViewModel> DisplayVisitHistoryViewModels { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.EditProfileCustomerViewModel> EditProfileCustomerViewModels { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.EditPlotDetailsViewModel> EditPlotDetailsViewModels { get; set; }

        public System.Data.Entity.DbSet<ParkingSystem.ViewModel.HomePageDisplayViewModel> HomePageDisplayViewModels { get; set; }
    }
}