﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParkingSystem.Models
{
    public class AreaAutoComplete
    {
        [Key]
        public int Id { get; set; }
        public string AreaName { get; set; }
    }
}