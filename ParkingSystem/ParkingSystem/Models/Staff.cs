﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParkingSystem.Models
{
    public class Staff
    {
        public int StaffId { get; set; }
        public int PlotId { get; set; }
        public int AreaId { get; set; }
        public int StaffStatus { get; set; }
    }
}