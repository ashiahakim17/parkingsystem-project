﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParkingSystem.Models
{
    public class PlotDetails
    {
        public int PlotId { get; set; }
        public string PlotName { get; set; }
        public int AreaId { get; set; }
        public string MobileNo { get; set; }
        public int PlotCapacity { get; set; }
        public string PlotStatus { get; set; }
        public int UserId { get; set; }
    }
}