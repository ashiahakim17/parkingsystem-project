﻿$(function () {
    $("#staffbookingGrid").jqGrid({
        url: "/Staff/GetBookingDetailsOfStaff",
        datatype: 'json',
        mtype: 'Get',
        colNames: ['Id', 'RegistrationId', 'PlotName', 'SlotId', 'EntryTime', 'ExitTime', 'TotalFare','CustomerId', 'BookerId'],
        colModel: [
            { key: true, hidden: true, name: 'Id', index: 'Id', editable: true },
            { key: false, name: 'RegistrationId', index: 'RegistrationId', editable: true },
            { key: false, name: 'PlotName', index: 'PlotName', editable: true,search: false, },
            { key: false, name: 'SlotId', index: 'SlotId', editable: true, search: false, },
            {
                key: false, name: 'EntryTime', index: 'EntryTime', editable: true, formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y h:i A' }, searchoptions: {

                    attr: { placeholder: 'mm/dd/yyyy' },
                    dataInit: function (elem) {
                        var self = this;
                        $(elem).datepicker({
                            dateFormat: 'yy-mm-dd',
                            autoSize: true,
                            changeYear: true,
                            changeMonth: true,
                            showButtonPanel: true,
                            showWeek: true,
                            onSelect: function () {
                                    $(this).trigger('change');
                                
                            }
                        });
                    }

                }
            },
            { key: false, name: 'ExitTime', index: 'ExitTime', editable: true, search: false, formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y h:i A' } },
            { key: false, name: 'TotalFare', index: 'TotalFare', editable: true, search: false },
            { key: false, name: 'UserName', index: 'UserName', editable: true, search: true },
            { key: false, name: 'BookerId', index: 'BookerId', editable: true, search: false }],
        pager: jQuery('#jqControls'),
        rowNum: 10,
        rowList: [10, 20, 30, 40, 50],
        height: '100%',
        viewrecords: true,
        caption: 'Booking History',
        emptyrecords: 'No Details added',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: '0'
        },
        autowidth: true,
        multiselect: false
    }).navGrid('#jqControls', { edit: false, add: false, del: false, search: true, refresh: true },
        {
            zIndex: 100,
            url: '#',
            closeOnEscape: true,
            closeAfterEdit: true,
            recreateForm: true,
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                }
            }
        },
        {
            zIndex: 100,


            url: '#',
            closeOnEscape: true,
            closeAfterAdd: true,
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                }
            }
        },
        {
            zIndex: 100,
            url: "#",
            closeOnEscape: true,
            closeAfterDelete: true,
            recreateForm: true,
            msg: "Are you sure you want to delete ... ? ",
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                }
            }
        },
    {
        zIndex: 100,
        caption: "Search",
        closeOnEscape: true,
        closeAfterSearch: true,
        sopt: ['eq']

    });
});