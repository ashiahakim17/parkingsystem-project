﻿function Check(url) {
    $("#Email").autocomplete({
        source: url
    })
}
function doAutoCompleteForCustomer(url) {
    var city = $("#city").val();
    var newurl = url + city;
    $("#AreaName").autocomplete({
        source: newurl,
        select: function (event, response) {
            $.ajax({
                url: "Customer/GetPlotDetails",
                dataType: "json",
                data: {
                    plotName: response.item.value,
                    cityName: $("#city").val()
                },
                success: function (data) {

                    window.location = 'Customer/DisplaySlot?id=' + data;
                }
            });
        }
    });
}

function doAutoComplete(url) {
    var city = $("#city").val();
    var newurl = url + city;
    $("#AreaName").autocomplete({
        source: newurl,
        select: function (event, response) {
            $.ajax({
                url: "Home/PlotDetailsForPopUp",
                dataType: "json",
                data: {
                    plotName: response.item.value,
                    cityName: $("#city").val()
                },
                success: function (data) {
                    processDataMethod(data);
                    response(data);
                }
            });
        }
    });
}
function CustomerValidation() {
    if (/[A-Z]/.test($("#cmobileno").val()) || /[a-z]/.test($("#cmobileno").val())) {
        $("#dmobileno").text("Enter valid Mobile No");
        event.preventDefault();
    }
}
function PlotterValidation() {
    if (/[A-Z]/.test($("#pmobileno").val()) || /[a-z]/.test($("#pmobileno").val())) {
        $("#pdmobileno").text("Enter valid Mobile No");
        event.preventDefault();
    }
}
function processDataMethod(data) {
    loadPopupBox();
    var output = $("#hiddiv");
    output.empty();
    for (var i = 0; i < data.length ; i++) {
        var plot = data[i];
        output.append("Plot Name:" + " " + plot.PlotName + "<hr>" + "Mobile Number:" + " " + plot.MobileNo + "<hr>" + "Owner Name:" + " " + plot.OwnerName + "<hr>" + "Capacity:" + " " + plot.Capacity);
    }
    $('#popupBoxClose').click(function () {
        unloadPopupBox();
    });
}

function unloadPopupBox() {// TO Unload the Popupbox
    $("#AreaName").val("");
    $('#popup_box').fadeOut("slow");
}

function loadPopupBox() {    // To Load the Popupbox
    $('#popup_box').fadeIn("slow");
}

$("#registered").on('click', function () {
    if ($(this).is(':checked')) {
        $("#custemail").show();
        $("#mobileno").hide();
    } else {
        $("#custemail").hide();
        $("#mobileno").show();
    }
});

