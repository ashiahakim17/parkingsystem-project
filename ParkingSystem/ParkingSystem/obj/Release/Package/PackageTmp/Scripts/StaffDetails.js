﻿$(function () {
    $("#staffGrid").jqGrid({
        url: "/Plot/GetStaffDetailsOfPlotter",
        datatype: 'json',
        mtype: 'Get',
        colNames: ['Id', 'StaffName', 'PlotName', 'UserName', 'Address'],
        colModel: [
            { key: true, name: 'Id', index: 'Id', search: false },
            { key: false, name: 'StaffName', index: 'StaffName', editable: true },
            { key: false, name: 'PlotName', index: 'PlotName', editable: false, cellEdit: true },
            { key: false, name: 'UserName', index: 'UserName', editable: true },
            { key: false, name: 'Address', index: 'Address', editable: true, search: false }],

        rowNum: 10,
        rowList: [10, 20, 30, 40, 50],
        pager: jQuery('#jqControls'),
        height: '100%',
        viewrecords: true,
        ignoreCase: true,
        loadonce: true,
        caption: 'Staff Details',
        emptyrecords: 'No Details added',
        jsonReader: {
            root: "rows",
            records: "records",
            repeatitems: false,
            Id: '0'
        },
        autowidth: true,

        multiselect: false
    }).navGrid('#jqControls', { edit: false, add: false, del: false, search: true, refresh: true },
        {
            zIndex: 100,
            url: '#',
            closeOnEscape: true,
            closeAfterEdit: true,
            recreateForm: true,
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                }
            }
        },
        {
            zIndex: 100,
            url: '#',
            mtype: 'POST',
            closeOnEscape: true,
            closeAfterAdd: true,
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                }
            }
        },
        {
            zIndex: 100,
            url: "#",
            closeOnEscape: true,
            closeAfterDelete: true,
            recreateForm: true,
            msg: "Are you sure you want to delete... ? ",
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                }
            }
        },
            {
                zIndex: 100,
                caption: "Search",
                closeOnEscape: true,
                closeAfterSearch: true,
                sopt: ['eq']
            });
});
