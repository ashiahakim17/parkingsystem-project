﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class HomePageDisplayViewModel
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Plot Name")]
        public string PlotName { get; set; }

        [Display(Name = "Capacity")]
        public string Capacity { get; set; }

        [Display(Name = "Contact Number")]
        public string MobileNo { get; set; }

        [Display(Name = "Owner Name")]
        public string OwnerName { get; set; }
    }
}