﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace ParkingSystem.ViewModel
{
    public class HomePageViewModel
    {
        [Key]
        public int Id { get; set; }

        [NotMapped]
        public SelectList CityList { get; set; }

        public string CityName { get; set; }
        public int AreaId { get; set; }

        public string AreaName { get; set; }

        [NotMapped]
        public SelectList AreaList { get; set; }

        public string SelectedArea { get; set; }
    }
}