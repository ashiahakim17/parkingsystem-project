﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class DisplayPlotNamePerPerson
    {
        [Key]
        public int Id { get; set; }

        public int PlotId { get; set; }
        public string PlotName { get; set; }
    }
}