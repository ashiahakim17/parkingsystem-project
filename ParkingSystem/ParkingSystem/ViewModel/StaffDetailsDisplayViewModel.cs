﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class StaffDetailsDisplayViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string StaffName { get; set; }

        [Required]
        public string PlotName { get; set; }

        [Required]
        public string UserName { get; set; }
    }
}