﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class SlotRegistrationViewModel
    {
        [Key]
        public int RegistrationId { get; set; }

        public int SlotId { get; set; }

        [Display(Name = "Car Number")]
        [Required(ErrorMessage = "*Required")]
        public string CarNumber { get; set; }

        [Display(Name = "Car Name")]
        public string CarName { get; set; }

        [Display(Name = "Mobile No")]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "*Invalid Mobile No")]
        [RegularExpression("([0-9]+)", ErrorMessage = "*Invalid Mobile No")]
        public string MobileNo { get; set; }

        public int BookerId { get; set; }

        [Display(Name = "Email Id")]
        [EmailAddress(ErrorMessage = "*Invalid EmailId")]
        public string UserName { get; set; }
    }
}