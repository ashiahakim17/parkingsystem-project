﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace ParkingSystem.ViewModel
{
    public class StaffRegisterViewModel
    {
        [Key]
        public int StaffId { get; set; }

        [Display(Name = "Name")]
        [Required]
        public string StaffName { get; set; }

        [Display(Name = "Plot")]
        public int PlotId { get; set; }

        [Display(Name = "Plot")]
        public string PlotName { get; set; }

        [NotMapped]
        public SelectList PlotList { get; set; }

        public string SelectedPlot { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Mobile No")]
        [Required]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "Mobile No length should be 10")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Only Numeric Values allowed")]
        public string MobileNo { get; set; }

        [Display(Name = "Email Id")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        [Required]
        public string UserName { get; set; }

        [Display(Name = "Password")]
        [Required]
        public string Password { get; set; }

        public string Hash { get; set; }
        public int PlotterId { get; set; }
    }
}