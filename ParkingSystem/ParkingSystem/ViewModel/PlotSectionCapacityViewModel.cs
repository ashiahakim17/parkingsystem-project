﻿namespace ParkingSystem.ViewModel
{
    public class PlotSectionCapacityViewModel
    {
        public int id { get; set; }
        public string SectionName { get; set; }
        public int Capacity { get; set; }
        public int PlotId { get; set; }
    }
}