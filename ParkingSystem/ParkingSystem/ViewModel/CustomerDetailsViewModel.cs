﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class CustomerDetailsViewModel
    {
        [Key]
        public int CustomerId { get; set; }

        [Display(Name = "Name")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Address")]
        [Required]
        public string Address { get; set; }

        [Display(Name = "Mobile No")]
        [MaxLength(10)]
        [Required]
        public string MobileNo { get; set; }

        [Display(Name = "UserName")]
        [Required]
        public string UserName { get; set; }

        [Display(Name = "Password")]
        [Required]
        public string Password { get; set; }
    }
}