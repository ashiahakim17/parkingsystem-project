﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace ParkingSystem.ViewModel
{
    public class EditPlotDetailsViewModel
    {
        [Key]
        public int Id { get; set; }

        public int PlotId { get; set; }

        [Display(Name = "Plot Name")]
        public string PlotName { get; set; }

        [Display(Name = "Area")]
        public string AreaName { get; set; }

        [NotMapped]
        public SelectList AreaList { get; set; }

        public string SelectedArea { get; set; }

        [Display(Name = "City")]
        public string CityName { get; set; }

        [NotMapped]
        public SelectList CityList { get; set; }
    }
}