﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class DisplayPaymentCloseDetails
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Registration Id")]
        public int RegistrationId { get; set; }

        public int SlotId { get; set; }

        [Display(Name = "Car Name")]
        public string CarName { get; set; }

        [Display(Name = "Car Number")]
        public string CarNumber { get; set; }

        public int CustomerId { get; set; }

        [Display(Name = "Total Fare")]
        public int TotalFare { get; set; }
    }
}