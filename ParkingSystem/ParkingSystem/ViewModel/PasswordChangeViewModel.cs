﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class PasswordChangeViewModel
    {
        [Required(ErrorMessage="*Required")]
        [Display(Name = "New Password")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required(ErrorMessage="*Required")]
        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "*Not Matched")]
        public string ConfirmPassword { get; set; }
        public string UserName { get; set; }
    }
}