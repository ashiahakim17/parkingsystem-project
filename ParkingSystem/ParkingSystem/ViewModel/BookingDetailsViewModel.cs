﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class BookingDetailsViewModel
    {
        [Key]
        public int Id { get; set; }

        public int RegistrationId { get; set; }
        public string PlotName { get; set; }
        public int SlotId { get; set; }
        public string CarNumber { get; set; }
        public DateTime EntryTime { get; set; }
        public DateTime? ExitTime { get; set; }
        public int? TotalFare { get; set; }
        public int BookerId { get; set; }
        public string UserName { get; set; }
    }
}