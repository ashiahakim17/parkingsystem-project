﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class DisplayCapacityForEditViewModel
    {
        [Key]
        public int PlotSectionId { get; set; }

        public int PlotId { get; set; }
        public string SectionName { get; set; }
        public int Capacity { get; set; }
    }
}