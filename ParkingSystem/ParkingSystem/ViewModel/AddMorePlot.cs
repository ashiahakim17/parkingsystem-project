﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace ParkingSystem.ViewModel
{
    public class AddMorePlot
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Plot Name")]
        [Required(ErrorMessage = "*Required")]
        public string PlotName { get; set; }

        [Required]
        [Display(Name = "Area")]
        public int AreaId { get; set; }

        [Display(Name = "Area")]
        public string AreaName { get; set; }

        [NotMapped]
        public SelectList AreaList { get; set; }

        public string SelectedArea { get; set; }

        [Display(Name = "City")]
        [Required]
        public string PlotCity { get; set; }

        [NotMapped]
        public SelectList CityList { get; set; }

        public int PersonId { get; set; }
    }
}