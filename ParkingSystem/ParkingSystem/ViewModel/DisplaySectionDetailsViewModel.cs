﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class DisplaySectionDetailsViewModel
    {
        [Key]
        public int PlotSectionId { get; set; }
    }
}