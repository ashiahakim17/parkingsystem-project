﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class CustomerRegisterViewModel
    {
        [Key]
        public int CustomerId { get; set; }

        [Display(Name = "Name")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Mobile No")]
        [Required]
        [RegularExpression("([0-9]+)", ErrorMessage = "Only Numeric Values allowed")]
        public string MobileNo { get; set; }

        [Display(Name = "Email Id")]
        [Required]
        [EmailAddress]
        public string UserName { get; set; }

        [Display(Name = "Password")]
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string Hash { get; set; }
    }
}