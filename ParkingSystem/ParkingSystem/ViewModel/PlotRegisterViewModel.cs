﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace ParkingSystem.ViewModel
{
    public class PlotRegisterViewModel
    {
        [Key]
        public int PlotId { get; set; }

        [Display(Name = "Plot Name")]
        [Required]
        public string PlotName { get; set; }

        [Display(Name = "City")]
        [Required]
        public string CityName { get; set; }

        [NotMapped]
        public SelectList CityList { get; set; }

        public int AreaId { get; set; }

        [Display(Name = "Area")]
        public string AreaName { get; set; }

        [NotMapped]
        public SelectList AreaList { get; set; }

        public string SelectedArea { get; set; }

        [Display(Name = "Owner Name")]
        [Required]
        public string OwnerName { get; set; }

        [Display(Name = " Owner Address")]
        public string Address { get; set; }

        [Display(Name = "Mobile No")]
        [Required]
        [RegularExpression("([0-9]+)", ErrorMessage = "Only Numeric Values allowed")]
        public string MobileNo { get; set; }

        [Display(Name = "Email Id")]
        [Required]
        [DataType(DataType.EmailAddress)]
        public string UserName { get; set; }

        [Display(Name = "Password")]
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string Hash { get; set; }
    }
}