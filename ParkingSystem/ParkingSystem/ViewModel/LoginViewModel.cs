﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class LoginViewModel
    {
        [Key]
        public int LoginId { get; set; }

        [Display(Name = "Email Id")]
        [Required(ErrorMessage = "*Required")]
        [DataType(DataType.EmailAddress, ErrorMessage = "*Invalid EmailId")]
        public string UserName { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "*Required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}