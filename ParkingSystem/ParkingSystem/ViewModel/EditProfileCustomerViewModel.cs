﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class EditProfileCustomerViewModel
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Name")]
        public string PersonName { get; set; }

        [Display(Name = "Mobile No")]
        public string MobileNo { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Email Id")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        public string UserName { get; set; }
    }
}