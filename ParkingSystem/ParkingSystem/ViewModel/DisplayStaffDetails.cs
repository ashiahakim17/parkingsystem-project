﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class DisplayStaffDetails
    {
        [Key]
        public int Id { get; set; }

        public string StaffName { get; set; }
        public string PlotName { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        public string UserName { get; set; }

        public string Address { get; set; }
        public int PlotterId { get; set; }
    }
}