﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class DisplaySlotNamePerSection
    {
        [Key]
        public int SlotId { get; set; }

        public int SlotIdPerSection { get; set; }
        public string PlotName { get; set; }
        public string SectionName { get; set; }
        public int Capacity { get; set; }
        public int PlotId { get; set; }
        public int Status { get; set; }
    }
}