﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class DisplayVisitHistoryViewModel
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Registration Id")]
        public int RegistrationId { get; set; }

        [Display(Name = "Plot")]
        public string PlotName { get; set; }

        [Display(Name = "Entry Time")]
        public DateTime EntryTime { get; set; }

        [Display(Name = "Exit Time")]
        public DateTime ExitTime { get; set; }

        [Display(Name = "Total Fare")]
        public int TotalFare { get; set; }
    }
}