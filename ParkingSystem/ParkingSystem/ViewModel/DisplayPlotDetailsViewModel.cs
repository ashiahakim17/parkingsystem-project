﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.ViewModel
{
    public class DisplayPlotDetailsViewModel
    {
        [Key]
        public int PlotId { get; set; }

        public string PlotName { get; set; }
    }
}