﻿function Check(url) {
    $("#Email").autocomplete({
        source: url
    })
}
function doAutoCompleteForCustomer(url) {
    var city = $("#city").val();
    var newurl = url + city;
    $("#AreaName").autocomplete({
        source: newurl,
        select: function (event, response) {
            $.ajax({
                url: "../Customer/GetPlotDetails",
                dataType: "json",
                data: {
                    plotName: response.item.value,
                    cityName: $("#city").val()
                },
                success: function (data) {
                    $("#mainsectionhomepage").load("../Customer/DisplaySlot?id=" + data);
                    // window.location = 'Customer/DisplaySlot?id=' + data;
                }
            });
        }
    });
}

function doAutoComplete(url) {
    var city = $("#city").val();
    var newurl = url + city;
    $("#AreaName").autocomplete({
        source: newurl,
        select: function (event, response) {
            $.ajax({
                url: "Home/PlotDetailsForPopUp",
                dataType: "json",
                data: {
                    plotName: response.item.value,
                    cityName: $("#city").val()
                },
                success: function (data) {
                    processDataMethod(data);
                }
            });
        }
    });
}
function CustomerValidation() {
    if (/[A-Z]/.test($("#cmobileno").val()) || /[a-z]/.test($("#cmobileno").val())) {
        $("#dmobileno").text("Enter valid Mobile No");
        event.preventDefault();
    }
}
function PlotterValidation() {
    if (/[A-Z]/.test($("#pmobileno").val()) || /[a-z]/.test($("#pmobileno").val())) {
        $("#pdmobileno").text("Enter valid Mobile No");
        event.preventDefault();
    }
}
function processDataMethod(data) {
    loadPopupBox();
    var output = $("#hiddiv");
    output.empty();
    for (var i = 0; i < data.length ; i++) {
        var plot = data[i];
        output.append("Plot Name:" + " " + plot.PlotName + "<hr>" + "Mobile Number:" + " " + plot.MobileNo + "<hr>" + "Owner Name:" + " " + plot.OwnerName + "<hr>" + "Capacity:" + " " + plot.Capacity);
    }
    $('#popupBoxClose').click(function () {
        unloadPopupBox();
    });
}

function unloadPopupBox() {// TO Unload the Popupbox
    $("#AreaName").val("");
    $('#popup_box').fadeOut("slow");
}

function loadPopupBox() {    // To Load the Popupbox
    $('#popup_box').fadeIn("slow");
}


function SlotDisplay(PlotId) {
    $.ajax({
        url: "Plot/DisplaySlot",
        dataType: "html",
        method: "post",
        data: {
            plotId: PlotId
        },
        success: function (partialview) {
            $('#mainsectionhomepage').html(partialview);
        }
    });
}
function BookSlot(SlotId) {
    $.ajax({
        url: "Plot/BookSlot",
        dataType: "html",
        method: "get",
        data: {
            slotId: SlotId
        },
        success: function (partialview) {
            var url = "Plot/Check";
            $('#mainsectionhomepage').html(partialview);
            $("#registered").on('click', function () {
                if ($(this).is(':checked')) {
                    $("#custemail").show();
                    $("#mobileno").hide();
                } else {
                    $("#custemail").hide();
                    $("#mobileno").show();
                }
            });
            Check(url);
        }
    });
}

function loginval() {
    if ($("#UserName").val().length == 0) {
        $("#uname").text("*required");
        event.preventDefault();
    }
    if ($("#Password").val().length == 0) {
        $("#pass").text("*required");
        event.preventDefault();
    }

    var a = $('#UserName').val();
    var b = $('#Password').val();
    $.get('../Home/LoginVal',
                { username: a, password: b, returnUrl: "" }, function (response) {
                    if (response != "Invalid") {
                        if (response == "Staff") {
                            window.location = 'Staff'
                        }
                        if (response == "Plotter") {
                            window.location = 'Plot'
                        }
                        if (response == "Customer") {
                            window.location = 'Customer'
                        }
                    }
                    else {
                        $("#loginmessage").text("*Invalid Username and Password");
                        event.preventDefault();
                    }
                });
}
function CloseSlot(SlotId) {
    $.ajax({
        url: "Plot/CloseSlot",
        dataType: "html",
        method: "get",
        data: {
            slotId: SlotId
        },
        success: function (partialview) {       
            $('#mainsectionhomepage').html(partialview);
        }
    });
}


function CloseSlotPayment(SlotId) {
    $.ajax({
        url: "Plot/CloseSlotPayment",
        dataType: "html",
        method: "get",
        data: {
            slotId: SlotId
        },
        success: function (partialview) {
            $('#mainsectionhomepage').html(partialview);
        }
    });
}
function EditPlotDetails(PlotId) {
    $.ajax({
        url: "Plot/EditPlotDetails",
        dataType: "html",
        method: "get",
        data: {
            plotId: PlotId
        },
        success: function (partialview) {
            $('#mainsectionhomepage').html(partialview);
        }
    });
}

function AjaxBegin() {
    $('#div_loading').show();

}
function AjaxComplete() {
    $("#div_loading").hide();
}