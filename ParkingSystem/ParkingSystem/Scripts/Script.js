﻿$(function () {
    var plotId = $("#plotId").val();
    $("#plotregisterGrid").jqGrid({
        url: "/Home/GetSectionCapacityOfPlot?id=" + plotId,
        datatype: 'json',
        mtype: 'Get',
        colNames: ['ID', 'SectionName', 'Capacity'],
        colModel: [
            { key: true, hidden: true, name: 'ID', index: 'ID', editable: true },
            { key: false, name: 'SectionName', index: 'SectionName', editable: true },
            { key: false, name: 'Capacity', index: 'Capacity', editable: true }],
        pager: jQuery('#jqControls'),
        height: '100%',
        viewrecords: true,
        caption: 'Section Capacity',
        emptyrecords: 'No Details added',
        jsonReader: {
            root: "rows",
            records: "records",
            repeatitems: false,
            Id: '0'
        },
        autowidth: true,
        multiselect: false
    }).navGrid('#jqControls', { edit: false, add: true, del: false, search: false, refresh: true },
        {
            zIndex: 100,
            url: '/Student/Edit',
            closeOnEscape: true,
            closeAfterEdit: true,
            recreateForm: true,
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                }
            }
        },
        {
            zIndex: 100,

            url: '/Home/Create?id=' + plotId,
            mtype: 'POST',
            postData: {
                PlotId: $('#plotId').val()
            },

            closeOnEscape: true,
            closeAfterAdd: true,
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                }
            }
        },
        {
            zIndex: 100,
            url: "#",
            closeOnEscape: true,
            closeAfterDelete: true,
            recreateForm: true,
            msg: "Are you sure you want to delete... ? ",
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                }
            }
        });
});