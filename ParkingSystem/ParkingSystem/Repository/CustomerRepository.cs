﻿using Dapper;
using log4net;
using ParkingSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ParkingSystem.Repository
{
    public class CustomerRepository : DataConnection
    {
        private ILog Log = LogManager.GetLogger(typeof(CustomerRepository));

        //Adding Customer
        public void AddCustomer(CustomerRegisterViewModel model)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var customerDetails = new
                                          {
                                              @PersonName = model.Name,
                                              @MobileNo = model.MobileNo,
                                              @Address = model.Address,
                                              @UserName = model.UserName,
                                              @Password = model.Hash,
                                              @PersonStatus = 1,
                                              @CustomerStatus = 1
                                          };
                    con.Execute("CustomerDetailsSaved", customerDetails, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Retrival of PlotId from the autocomplete search
        public int GetSlotsForPlotAndArea(HomePageViewModel model)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var plotDetails = new
                                      {
                                          @CityName = model.CityName,
                                          @AreaName = model.AreaName
                                      };
                    return (con.ExecuteScalar<Int32>("PlotAreaAutocompleteIdRetrival", plotDetails, commandType: CommandType.StoredProcedure));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        // Retrival of Booking History of Customer
        public List<DisplayVisitHistoryViewModel> DisplayVisitRecords(int customerId)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var customerDetail = new
                                         {
                                             @CustomerId = customerId
                                         };
                    return (con.Query<DisplayVisitHistoryViewModel>("CustomerBookingHistory", customerDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Retrival of Customer Details
        public List<EditProfileCustomerViewModel> GetPersonDetailsForEditing()
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    return (con.Query<EditProfileCustomerViewModel>("CustomerDetailsForEditing", commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        // Retrival of Booking History by RegistrationId
        public List<DisplayVisitHistoryViewModel> GetBookingHistoryByRegistrationId(int personId, string searchTerm)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    int id;
                    bool result = int.TryParse(searchTerm, out id);
                    var bookingDetails = new
                                         {
                                             @CustomerId = personId,
                                             @RegistrationId = id
                                         };
                    return (con.Query<DisplayVisitHistoryViewModel>("CustomerBookingByRegistrationId", bookingDetails, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Customer Booking History By PlotName
        public List<DisplayVisitHistoryViewModel> GetBookingHistoryByPlotName(int personId, string searchTerm)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var bookingSearch = new
                                        {
                                            @CustomerId = personId,
                                            @PlotName = searchTerm
                                        };

                    return (con.Query<DisplayVisitHistoryViewModel>("CustomerBookingByPlotName", bookingSearch, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Customer Booking History By Date
        public List<DisplayVisitHistoryViewModel> GetBookingHistoryByEntryTime(int personId, string searchTerm)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var bookingSearch = new
                    {
                        @CustomerId = personId,
                        @EntryTime = searchTerm
                    };

                    return (con.Query<DisplayVisitHistoryViewModel>("CustomerBookingByEntryTime", bookingSearch, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Retrival of Customer EmailId Autocomplete
        public List<String> GetCustomerUserName(string term)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var userName = new
                                         {
                                             @UserName = term
                                         };
                    return (con.Query<string>("CustomerEmailAutoComplete", userName, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }
    }
}