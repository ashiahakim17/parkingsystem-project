﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ParkingSystem.Repository
{
    public class DataConnection
    {

        private string constr = ConfigurationManager.ConnectionStrings["Parking"].ConnectionString;
        private SqlConnection con;
        public SqlConnection GetConnection()
        {
            con = new SqlConnection(constr);
            con.Open();
            return con;

        }






    }
}