﻿using Dapper;
using log4net;
using ParkingSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ParkingSystem.Repository
{
    public class CommonRepository : DataConnection
    {
        private ILog Log = LogManager.GetLogger(typeof(CommonRepository));

        //Verify Logger
        public int CheckUserLogger(LoginViewModel model)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var loginDetails = new
                                       {
                                           @UserName = model.UserName,
                                           @Password = model.Password
                                       };
                    return (con.Query<int>("UserNamePasswordValidation", loginDetails, commandType: CommandType.StoredProcedure).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Get Role From PersonId
        public string GetRole(int user)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var userDetail = new
                                     {
                                         @PersonId = user
                                     };
                    return (con.Query<string>("RoleNameRetrival", userDetail, commandType: CommandType.StoredProcedure).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Update Person Record
        public void UpdatePersonRecords(EditProfileCustomerViewModel model)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var personDetail = new
                                       {
                                           @PersonId = model.Id,
                                           @PersonName = model.PersonName,
                                           @MobileNo = model.MobileNo,
                                           @Address = model.Address,
                                       };
                    con.Execute("PersonDetailsEditProfileUpdation", personDetail, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Plot Details For Home Page
        public List<HomePageDisplayViewModel> GetPlotDetailsForHomePage(int plotId)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var plotDetail = new
                                     {
                                         @PlotId = plotId
                                     };
                    return (con.Query<HomePageDisplayViewModel>("PlotDetailsHomePageRetrival", plotDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Verify User
        public int VerifyUser(int personId, string role)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var userDetail = new
                    {
                        @PersonId = personId,
                        @Role = role
                    };
                    return (con.Query<Int32>("UserValidation", userDetail, commandType: CommandType.StoredProcedure).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Retrival of City List
        public IEnumerable<HomePageViewModel> GetCityList()
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    return (con.Query<HomePageViewModel>("CityListRetrival", commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Retrival of Area List
        public IEnumerable<PlotRegisterViewModel> GetAreaList()
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    return (con.Query<PlotRegisterViewModel>("AreaListRetrival", commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Retrival of Plot Details for searched term
        public List<HomePageDisplayViewModel> GetPlotDetailsBySearchedTerm(HomePageViewModel model)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var plotDetail = new
                    {
                        @plotAreaName = model.AreaName,
                        @CityName = model.CityName
                    };
                    return (con.Query<HomePageDisplayViewModel>("PlotNameMatchedResult", plotDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Check for duplicacy of Email
        public int CheckValidEmail(string UserName)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var personEmail = new
                    {
                        @UserName = UserName
                    };
                    return (con.Query<int>("UserEmailDuplicacyCheck", personEmail, commandType: CommandType.StoredProcedure).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        public void PasswordChange(PasswordChangeViewModel model)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var personPassword = new
                    {
                        @UserName = model.UserName,
                        @Password=model.NewPassword
                    };
                    con.Execute("UserPasswordChange", personPassword, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }
    }
}