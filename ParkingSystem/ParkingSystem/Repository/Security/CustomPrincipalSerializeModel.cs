﻿using System.ComponentModel.DataAnnotations;

namespace ParkingSystem.Repository.Security
{
    public class CustomPrincipalSerializeModel
    {
        [Key]
        public int PersonId { get; set; }

        public string UserName { get; set; }
        public string RoleName { get; set; }
    }
}