﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace ParkingSystem.Repository.Security
{
    public enum Role
    {
        Customer,
        Staff,
        Plotter
    }

    public class CustomAuthorizeAttribute
        : AuthorizeAttribute
    {
        public IList<Role> Roles { get; set; }

        public CustomAuthorizeAttribute(params Role[] role)
        {
            this.Roles = role;
        }

        protected virtual CustomPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as CustomPrincipal; }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Request.IsAuthenticated)
            {
                foreach (var role in Roles)
                {
                    CommonRepository commonRepo = new CommonRepository();
                    int user = commonRepo.VerifyUser(CurrentUser.PersonId, role.ToString());
                    if (user > 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}