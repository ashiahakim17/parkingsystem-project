﻿using System.Linq;
using System.Security.Principal;

namespace ParkingSystem.Repository.Security
{
    public class CustomPrincipal : IPrincipal
    {
        public IIdentity Identity { get; private set; }

        public bool IsInRole(string role)
        {
            return roles.Any(r => role.Contains(r));
        }

        public CustomPrincipal(string username)
        {
            this.Identity = new GenericIdentity(username);
        }

        public int PersonId { get; set; }
        public string UserName { get; set; }
        public string roles { get; set; }
    }
}