﻿using Dapper;
using log4net;
using ParkingSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ParkingSystem.Repository
{
    public class StaffRepository : DataConnection
    {
        private ILog Log = LogManager.GetLogger(typeof(StaffRepository));

        // For Adding new staff
        public void AddStaff(StaffRegisterViewModel model)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var staffDetails = new
                                       {
                                           @PersonName = model.StaffName,
                                           @MobileNo = model.MobileNo,
                                           @Address = model.Address,
                                           @UserName = model.UserName,
                                           @Password = model.Hash,
                                           @PersonStatus = 1,
                                           @PlotName = model.SelectedPlot,
                                           @PlotterId = model.PlotterId
                                       };
                    con.Execute("StaffDetailsSaved", staffDetails, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Getting Plot List for the dropdown
        public IEnumerable<StaffRegisterViewModel> GetPlotList(int personId)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var personDetail = new
                               {
                                   @PersonId = personId
                               };
                    return (con.Query<StaffRegisterViewModel>("PlotNameDropdownList", personDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Get Plot Details for Person
        public List<DisplayPlotNamePerPerson> GetPlotDetailsForPerson(int personId)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var personDetail = new
                                       {
                                           @PersonId = personId
                                       };

                    return (con.Query<DisplayPlotNamePerPerson>("StaffPlotNameList", personDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Displays Fare When after closure of Slot
        public List<DisplayPaymentCloseDetails> DisplayFareWhenClose(int slotId)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var slotDetail = new
                                     {
                                         @SlotId = slotId
                                     };
                    return (con.Query<DisplayPaymentCloseDetails>("CustomerFareWhenClose", slotDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Get Slot Details of Staff
        public List<DisplaySlotNamePerSection> GetSlotDetailsOfStaff(int personId)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var plotDetail = new
                                     {
                                         @PersonId = personId
                                     };
                    return (con.Query<DisplaySlotNamePerSection>("PlotSectionDetailsRetrivalByStaff", plotDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Get Booking Details of Staff
        public List<BookingDetailsViewModel> GetBookingDetailsOfStaff(int personId)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var staffDetail = new
                                     {
                                         @PersonId = personId
                                     };
                    return (con.Query<BookingDetailsViewModel>("StaffBookingDetails", staffDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Get Booking Details By Registration Id
        public List<BookingDetailsViewModel> GetStaffBookingByReg(int personId, string searchString)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var staffDetail = new
                                     {
                                         @PersonId = personId,
                                         @RegistrationId = searchString
                                     };
                    return (con.Query<BookingDetailsViewModel>("StaffBookingByReg", staffDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Get Booking Details By Date
        public List<BookingDetailsViewModel> GetStaffBookingByDate(int personId, string searchString)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var staffDetail = new
                    {
                        @PersonId = personId,
                        @EntryTime = searchString
                    };
                    return (con.Query<BookingDetailsViewModel>("StaffBookingByDate", staffDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Get Staff Booking By UserName
        public List<BookingDetailsViewModel> GetStaffBookingByUserName(int personId, string searchString)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var staffDetail = new
                    {
                        @PersonId = personId,
                        @UserName = searchString
                    };
                    return (con.Query<BookingDetailsViewModel>("StaffBookingByUserName", staffDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }
    }
}