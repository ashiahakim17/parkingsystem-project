﻿using Dapper;
using log4net;
using ParkingSystem.Models;
using ParkingSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ParkingSystem.Repository
{
    public class PlotRepository : DataConnection
    {
        private ILog Log = LogManager.GetLogger(typeof(PlotRepository));

        //Adding the Plot
        public int AddPlot(PlotRegisterViewModel model)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var plotRegister = new
                                       {
                                           @PersonName = model.OwnerName,
                                           @MobileNo = model.MobileNo,
                                           @Address = model.Address,
                                           @UserName = model.UserName,
                                           @Password = model.Hash,
                                           @PersonStatus = 1,
                                           @PlotName = model.PlotName,
                                           @CityName = model.CityName,
                                           @AreaName = model.SelectedArea
                                       };
                    return (con.Query<Int32>("PlotDetailsSaved", plotRegister, commandType: CommandType.StoredProcedure).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Add Section Capacity
        public void EnterNewSectionCapacity(PlotSectionCapacityViewModel model)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var plotSection = new
                                      {
                                          @PlotId = model.PlotId,
                                          @SectionName = model.SectionName,
                                          @SectionCapacity = model.Capacity
                                      };
                    con.Execute("PlotCapacityDetailsSaved", plotSection, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Display Staff Details of Plotter
        public List<DisplayStaffDetails> GetStaffDetailsOfPlotter(int personId)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var personDetail = new
                                      {
                                          @PersonId = personId
                                      };
                    return (con.Query<DisplayStaffDetails>("StaffDetailsOfPlotter", personDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Get Area List For Dropdown
        public IEnumerable<PlotRegisterViewModel> GetAreaList()
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    return (con.Query<PlotRegisterViewModel>("AreaListRetrival", commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //To Autocomplete the PlotName,AreaName
        public List<AreaAutoComplete> GetAreaName(string term)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var searchTerm = new
                                     {
                                         @term = term
                                     };
                    return (con.Query<AreaAutoComplete>("GetAreaNameAutocomplete", searchTerm, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Retrival of Section Details
        public List<PlotSectionCapacityViewModel> GetSectionCapacityOfPlotEntered(int plotId)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var plotDetail = new
                                     {
                                         @PlotId = plotId
                                     };
                    return (con.Query<PlotSectionCapacityViewModel>("PlotSectionCapacityRetrival", plotDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Retrival of Slots Details for the Plot
        public List<DisplaySlotNamePerSection> GetSectionDetailsForPlot(int plotId)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var plotDetail = new
                                     {
                                         @PlotId = plotId
                                     };
                    return (con.Query<DisplaySlotNamePerSection>("PlotSectionDetailsRetrival", plotDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        // Retrival of Plot List based on Person
        public List<DisplayPlotNamePerPerson> GetPlotDetailsForPerson(int personId)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var personDetail = new
                                       {
                                           @PersonId = personId
                                       };
                    return (con.Query<DisplayPlotNamePerPerson>("PlotNamePerPersonList", personDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Slot Registration For Customer
        public void RegisterSlotForCustomer(SlotRegistrationViewModel model)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var slotRegister = new
                                       {
                                           @SlotId = model.SlotId,
                                           @BookerId = model.BookerId,
                                           @CarName = model.CarName,
                                           @CarNumber = model.CarNumber,
                                           @MobileNumber = model.MobileNo,
                                           @UserName = model.UserName
                                       };
                    con.Execute("CustomerSlotRegistration", slotRegister, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        // For Adding More Plot
        public int AddMorePlot(AddMorePlot model)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var plotDetails = new
                                      {
                                          @PersonId = model.PersonId,
                                          @PlotName = model.PlotName,
                                          @SelectedArea = model.SelectedArea,
                                          @PlotCity = model.PlotCity
                                      };
                    return (con.Query<Int32>("PlotAddedByPlotter", plotDetails, commandType: CommandType.StoredProcedure).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        // Retrival of PlotId From SlotId
        public int GetPlotIdFromSlotId(int slotId)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var slotDetail = new
                                     {
                                         @SlotId = slotId
                                     };
                    return (con.Query<Int32>("PlotIdRetrivalFromSlotId", slotDetail, commandType: CommandType.StoredProcedure).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        // Retrival of Plot Detail from Autocomplete
        public List<string> GetPlotNameAndArea(string term, string city)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var searchTerm = new
                                     {
                                         @Term = term,
                                         @CityName = city
                                     };
                    return (con.Query<string>("PlotAreaAutocompleteList", searchTerm, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        // Retrival of Plot Details For Editing
        public List<EditPlotDetailsViewModel> GetPlotDetailsForEditing()
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    return (con.Query<EditPlotDetailsViewModel>("PlotDetailsForEditing", commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        // Section Detail For Plot Registered
        public List<DisplayCapacityForEditViewModel> GetSectionCapacityOfPlotRegisterd(int plotId)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var plotDetail = new
                                     {
                                         @PlotId = plotId
                                     };
                    return (con.Query<DisplayCapacityForEditViewModel>("PlotSectionDetailsForEditing", plotDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //For Editing the entered Section details capacity
        public void EditPlotCapacityDetails(DisplayCapacityForEditViewModel model)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var sectionDetail = new
                                        {
                                            @PlotSectionId = model.PlotSectionId,
                                            @SectionName = model.SectionName,
                                            @SectionCapacity = model.Capacity
                                        };
                    con.Execute("PlotCapacityDetailsForEditing", sectionDetail, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //For Adding Section in Existing Plot
        public void AddSectionInExistingPlot(DisplayCapacityForEditViewModel model)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var sectionAdd = new
                                     {
                                         @PlotId = model.PlotId,
                                         @SectionName = model.SectionName,
                                         @SectionCapacity = model.Capacity
                                     };
                    con.Execute("PlotSectionDetailsAddedWhileEditing", sectionAdd, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //For Editing Plot Location Details
        public void EditPlotLocationDetails(EditPlotDetailsViewModel model)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var plotEdit = new
                                   {
                                       @PlotId = model.PlotId,
                                       @PlotName = model.PlotName,
                                       @AreaName = model.AreaName,
                                       @CityName = model.CityName
                                   };
                    con.Execute("PlotEditLocationDetails", plotEdit, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        public List<DisplayStaffDetails> GetStaffDetailsByStaffName(int personId, string searchString)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var personDetail = new
                                      {
                                          @PersonId = personId,
                                          @StaffName = searchString
                                      };
                    return (con.Query<DisplayStaffDetails>("StaffDetailsByStaffName", personDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        public List<DisplayStaffDetails> GetStaffDetailsByPlotName(int personId, string searchString)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var personDetail = new
                    {
                        @PersonId = personId,
                        @PlotName = searchString
                    };
                    return (con.Query<DisplayStaffDetails>("StaffDetailsByPlotName", personDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        public List<DisplayStaffDetails> GetStaffDetailsByUserName(int personId, string searchString)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var personDetail = new
                    {
                        @PersonId = personId,
                        @UserName = searchString
                    };
                    return (con.Query<DisplayStaffDetails>("StaffDetailsByUserName", personDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        // Retrival of Plot List based on Person
        public List<DisplayPlotNamePerPerson> GetPlotDetailsByPlotName(int personId, string search)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var plotDetail = new
                    {
                        @PersonId = personId,
                        @PlotName = search
                    };
                    return (con.Query<DisplayPlotNamePerPerson>("PlotNameBySearchTerm", plotDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Get PlotName in Dropdown List
        public List<string> GetPlotList(int personId)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var personDetail = new
                    {
                        @PersonId = personId
                    };
                    return (con.Query<string>("PlotNameDropdownList", personDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Retrival of Booking  Details Of Plotter
        public List<BookingDetailsViewModel> GetBookingDetailsOfPlotter(int personId)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var personDetail = new
                    {
                        @PersonId = personId
                    };
                    return (con.Query<BookingDetailsViewModel>("PlotterBookingDetails", personDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Booking Details by Entry Time
        public List<BookingDetailsViewModel> GetBookingDetailsByEntryTime(int personId, string searchString)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var personDetail = new
                    {
                        @PersonId = personId,
                        @EntryTime = searchString
                    };
                    List<BookingDetailsViewModel> bookingDetails = con.Query<BookingDetailsViewModel>("PlotterBookingDetailsByDate", personDetail, commandType: CommandType.StoredProcedure).ToList();
                    return bookingDetails;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Booking details By Plot Name
        public List<BookingDetailsViewModel> GetBookingDetailsByPlotName(int personId, string searchString)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var personDetail = new
                    {
                        @PersonId = personId,
                        @PlotName = searchString
                    };
                    return (con.Query<BookingDetailsViewModel>("PlotterBookingByPlotName", personDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }

        //Booking details By Registration Id
        public List<BookingDetailsViewModel> GetBookingDetailsByReg(int personId, string searchString)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    var personDetail = new
                    {
                        @PersonId = personId,
                        @RegistrationId = searchString
                    };
                    return (con.Query<BookingDetailsViewModel>("PlotterBookingByReg", personDetail, commandType: CommandType.StoredProcedure).ToList());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                throw;
            }
        }
    }
}